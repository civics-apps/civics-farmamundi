# Generated by Django 3.1.6 on 2021-02-15 10:12

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import djgeojson.fields
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('models', '0005_auto_20200306_1846'),
    ]

    operations = [
        migrations.AlterField(
            model_name='active',
            name='address',
            field=models.CharField(help_text='Dirección del activo. No es necesario que vuelvas a introducir la ciudad del activo.', max_length=200, null=True, verbose_name='Dirección'),
        ),
        migrations.AlterField(
            model_name='active',
            name='agent',
            field=multiselectfield.db.fields.MultiSelectField(choices=[('IP', 'Instituciones públicas'), ('CI', 'Ciudadanía')], help_text='Agente que mapea el activo', max_length=10, verbose_name='Agente'),
        ),
        migrations.AlterField(
            model_name='active',
            name='city',
            field=models.ForeignKey(help_text='Ciudad donde se encuentra el activo. Si no encuentras la ciudad en el desplegable usa el botón inferior para añadirla.', null=True, on_delete=django.db.models.deletion.SET_NULL, to='models.city', verbose_name='Ciudad'),
        ),
        migrations.AlterField(
            model_name='active',
            name='featured',
            field=models.BooleanField(blank=True, default=False, help_text='Indica si es un activo destacado', verbose_name='Destacado'),
        ),
        migrations.AlterField(
            model_name='active',
            name='perspective',
            field=models.CharField(choices=[('EQ', 'Equidad'), ('PA', 'Participación'), ('FC', 'Fortalecimiento comunitario'), ('OT', 'Otra')], help_text='Perspectiva principal del activo. Definirá el icono del activo', max_length=2, verbose_name='Perspectiva principal'),
        ),
        migrations.AlterField(
            model_name='active',
            name='perspective_alt',
            field=multiselectfield.db.fields.MultiSelectField(blank=True, choices=[('EQ', 'Equidad'), ('PA', 'Participación'), ('FC', 'Fortalecimiento comunitario'), ('OT', 'Otra')], help_text='Otras perspectivas del activo. No hace falta que repitas la principal', max_length=10, verbose_name='Otras perspectivas'),
        ),
        migrations.AlterField(
            model_name='active',
            name='position',
            field=djgeojson.fields.PointField(help_text='Tras añadir ciudad y dirección puedes ubicar el activo pulsando el botón inferior y ajustando la posición del marcador posteriormente.', null=True, verbose_name='Ubicación'),
        ),
        migrations.AlterField(
            model_name='active',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='Gestora o gestor'),
        ),
        migrations.AlterField(
            model_name='active',
            name='website',
            field=models.URLField(blank=True, help_text='Puedes enlazar una web que ayude a conocer mejor el activo.', null=True, verbose_name='Website'),
        ),
    ]
