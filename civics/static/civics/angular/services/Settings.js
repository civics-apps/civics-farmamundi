angular.module('civics.settings', [])

.service('Settings', function($http){

      this.map_defaults = {
          center: {
              lat  : 37.1879,
              lng  : -4.2637,
              zoom : 8,
          },
          defaults: {
              markerZoomAnimation : true,
              zoomAnimation       : true,
              scrollWheelZoom     : true,
              minZoom             : 3,
              maxZoom             : 18,
          }
      };

      this.map_layers = {
          baselayers: {
              mapnik : {
                  name         : 'Open Street Map',
                  url          : 'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
                  layerOptions : {
                       attribution : '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                  },
                  type : 'xyz',
              },
              toner : {
                name         : 'Toner',
                url          : 'https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png',
                layerOptions : {
                  attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.',
                },
                type: 'xyz',
              },
          },
      };

      this.map_controls = {
          scale: true,
          custom: [
              L.control.locate({ follow: true }),
          ],
      };

      return this;

});
